<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace theme_unige\hook;

/**
 * Hook callbacks for theme_unige.
 *
 * @package    theme_unige.
 * @copyright  2024, Université de Genève <moodle@unige.ch>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
final class add_htmlattributes {
    /**
     * Add custom attributes to the html tag.
     *
     * @param array $attributes
     * @return array
     */
    public static function callback(\core\hook\output\before_html_attributes $hook): void {
        global $PAGE;

        if ($PAGE->theme->name !== 'unige') {
            return;
        } else {
            $hook->add_attribute('data-theme', get_user_preferences('accessibilitystyles_sitecolor', 'light'));
        }
    }
}
