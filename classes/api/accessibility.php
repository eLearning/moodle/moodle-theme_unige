<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Accessibility API endpoints
 *
 * @package    theme_unige
 * @copyright  2023, Université de Genève <moodle@unige.ch>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace theme_unige\api;

use core_external\external_api;
use core_external\external_function_parameters;
use core_external\external_single_structure;
use core_external\external_value;

/**
 * Accessibility API endpoints class
 *
 * @package    theme_unige
 * @copyright  2023, Université de Genève <moodle@unige.ch>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class accessibility extends external_api {
    /**
     * Site color endpoint parameters definition
     *
     * @return external_function_parameters
     */
    public static function sitecolor_parameters() {
        return new external_function_parameters([
            'color' => new external_value(PARAM_RAW, 'The colorscheme value'),
        ]);
    }

    /**
     * Site color endpoint implementation
     *
     * @param array $color
     *
     * @return array
     *
     * @throws \coding_exception
     * @throws \invalid_parameter_exception
     */
    public static function sitecolor($color): array {
        $params = self::validate_parameters(self::sitecolor_parameters(), ['color' => $color]);

        $newsitecolor = null;
        $allowedcolor = [
            'light',
            'dark',
        ];

        $newsitecolor = (in_array($params['color'], $allowedcolor)) ? $params['color'] : $allowedcolor[0];

        if (isloggedin() && !isguestuser()) {
            set_user_preference('accessibilitystyles_sitecolor', $newsitecolor);
        }

        return ['success' => true];
    }

    /**
     * Site color endpoint return definition
     *
     * @return external_single_structure
     */
    public static function sitecolor_returns() {
        return new external_single_structure([
            'success' => new external_value(PARAM_BOOL, 'Operation response')
        ]);
    }

    /**
     * Site font endpoint parameters definition
     *
     * @return external_function_parameters
     */
    public static function sitefont_parameters() {
        return new external_function_parameters([
            'font' => new external_value(PARAM_RAW, 'The font value'),
        ]);
    }

    /**
     * Site font endpoint implementation
     *
     * @param array $font
     *
     * @return array
     *
     * @throws \coding_exception
     * @throws \invalid_parameter_exception
     */
    public static function sitefont($font): array {
        $params = self::validate_parameters(self::sitefont_parameters(), ['font' => $font]);

        $newsitefont = null;
        $allowedfont = [
            'default',
            'odafont',
        ];

        $newsitefont = (in_array($params['font'], $allowedfont)) ? $params['font'] : $allowedfont[0];

        if (isloggedin() && !isguestuser()) {
            set_user_preference('accessibilitystyles_sitefont', $newsitefont);
        }

        return ['success' => true];
    }

    /**
     * Site font endpoint return definition
     *
     * @return external_single_structure
     */
    public static function sitefont_returns() {
        return new external_single_structure([
            'success' => new external_value(PARAM_BOOL, 'Operation response')
        ]);
    }
}
