<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Coursebox content renderable.
 *
 * @package    theme_unige
 * @copyright  2023, Université de Genève <moodle@unige.ch>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace theme_unige\output\course;
defined('MOODLE_INTERNAL') || die();

use core_course_renderer;
use theme_unige\output\core\course_renderer;
use renderable;
use renderer_base;
use templatable;
use stdClass;

class coursebox extends course_renderer implements renderable, templatable {
    private $course = null;
    private $chelper = null;
    private $additionalclasses = null;
    private $renderer = null;

    public function __construct(\coursecat_helper $chelper, \core_course_list_element $course, course_renderer $renderer, $additionalclasses = '') {
        $this->chelper = $chelper;
        $this->course = $course;
        $this->additionalclasses = $additionalclasses;
        $this->renderer = $renderer;
    }

    /**
     * Export this data so it can be used as the context for a mustache template.
     *
     * @param renderer_base $output
     * @return stdClass
     */
    public function export_for_template(renderer_base $output): array {
        $templatecontext = [];

        if ($this->chelper->get_show_courses() < self::COURSECAT_SHOW_COURSES_EXPANDED) {
            $templatecontext['collapsed'] = true;
        }

        $templatecontext['name'] = $output->course_name($this->chelper, $this->course);
        $templatecontext['enrolment_icons'] = $output->course_enrolment_icons($this->course);
        $templatecontext['data_type'] = self::COURSECAT_TYPE_COURSE;
        $templatecontext['coursebox_content'] = (new coursebox_content($this->chelper, $this->course, $this->renderer))
                                                    ->export_for_template($output);
        $templatecontext['additionalclasses'] = $this->additionalclasses;
        $templatecontext['shortname'] = $this->course->shortname;
        $templatecontext['course'] = $this->course;

        return $templatecontext;
    }
}
