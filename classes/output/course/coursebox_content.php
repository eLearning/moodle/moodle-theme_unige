<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Coursebox content renderable.
 *
 * @package    theme_unige
 * @copyright  2023, Université de Genève <moodle@unige.ch>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace theme_unige\output\course;
defined('MOODLE_INTERNAL') || die();

use theme_unige\output\core\course_renderer;
use renderable;
use renderer_base;
use templatable;
use stdClass;
use moodle_page;

class coursebox_content extends course_renderer implements renderable, templatable {
    private $course = null;
    private $chelper = null;
    private $renderer = null;

    public function __construct(\coursecat_helper $chelper, \core_course_list_element $course, course_renderer $renderer) {
        $this->chelper = $chelper;
        $this->course = $course;
        $this->renderer = $renderer;
    }

    /**
     * Export this data so it can be used as the context for a mustache template.
     *
     * @param renderer_base $output
     * @return stdClass
     */
    public function export_for_template(renderer_base $output): array {
        $templatecontext = [];
        $templatecontext['overviewfiles'] = $this->renderer->course_overview_files($this->course);
        $templatecontext['summary'] = $this->renderer->course_summary($this->chelper, $this->course);
        $templatecontext['contacts'] = $this->renderer->course_contacts($this->course);
        $templatecontext['categoryname'] = $this->renderer->course_category_name($this->chelper, $this->course);
        $templatecontext['customfields'] = $this->renderer->course_custom_fields($this->course);

        return $templatecontext;
    }
}
