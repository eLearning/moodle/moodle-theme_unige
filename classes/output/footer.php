<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace theme_unige\output;

use renderable;
use renderer_base;
use templatable;
use theme_config;
use custom_menu;
use moodle_url;

/**
 * more menu navigation renderable
 *
 * @package     unige
 * @category    navigation
 * @copyright   2023, Université de Genève <moodle@unige.ch>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class footer implements renderable, templatable {
    /** @var moodle_page $page the moodle page that the navigation belongs to */
    private $page = null;
    /**
     * @var \stdClass $theme The theme object.
     */
    protected $theme;

    /**
     * primary constructor.
     * @param \moodle_page $page
     */
    public function __construct($page) {
        $this->page = $page;
        $this->theme = theme_config::load('unige');
    }

    /**
     * Magic method to get theme settings
     *
     * @param string $name
     *
     * @return false|string|null
     */
    public function __get(string $name) {
        if (empty($this->theme->settings->$name)) {
            return false;
        }

        return $this->theme->settings->$name;
    }

    /**
     * Parse the footer links and return an array of custom_menu_item
     *
     * @param  mixed $content
     * @param  renderer_base $output
     * @return array
     */
    protected function convert_footer_links($content, renderer_base $output): array {
        $custommenu = new custom_menu();
        $linkscollection = $custommenu->convert_text_to_menu_nodes($content, current_language());

        $links = [];

        foreach ($linkscollection as $link) {
            $links[] = $link->export_for_template($output);
        }

        return $links;
    }

    protected function get_accreditation_links(): array {
        $context = \context_system::instance();
        $fs = get_file_storage();
        $files = $fs->get_area_files($context->id, 'theme_unige', 'accreditationimage', 0, 'itemid, filepath, filename', false);

        $accreditations = [];
        foreach ($files as $file) {
            $url = moodle_url::make_pluginfile_url(
                $file->get_contextid(),
                $file->get_component(),
                $file->get_filearea(),
                0,
                $file->get_filepath(),
                $file->get_filename()
            );
            $accreditations[] = ['url' => $url->out(false)];
        }

        return $accreditations;
    }

    /**
     * Describe the renderable widget so it can be renderer by a mustache template.
     *
     * @param renderer_base $output
     * @return array
     */
    public function export_for_template(renderer_base $output) {
        global $CFG;

        $templatecontext = [];

        $settings = [
            'footercolumnleftdisplay',
            'footercolumnlefttitle',
            'footercolumnleftcontent',
            'footercolumnmiddledisplay',
            'footercolumnmiddletitle',
            'footercolumnmiddlecontent',
            'footercolumnrightdisplay',
            'footercolumnrighttitle',
            'footercolumnrightcontent',
            'footercolumnaccreditationsdisplay',
            'footercolumnaccreditationstitle',
            'footercolumnaccreditationscontent',
        ];

        foreach ($settings as $setting) {
            $templatecontext[$setting] = $this->$setting;
        }

        $templatecontext['footercolumnlefttitle'] = format_string($templatecontext['footercolumnlefttitle'], true);
        $templatecontext['footercolumnleftcontent'] = self::convert_footer_links(
            $templatecontext['footercolumnleftcontent'],
            $output
        );

        $templatecontext['footercolumnmiddletitle'] = format_string($templatecontext['footercolumnmiddletitle'], true);
        $templatecontext['footercolumnmiddlecontent'] = self::convert_footer_links(
            $templatecontext['footercolumnmiddlecontent'],
            $output
        );

        $templatecontext['footercolumnrighttitle'] = format_string($templatecontext['footercolumnrighttitle'], true);
        $templatecontext['footercolumnrightcontent'] = self::convert_footer_links(
            $templatecontext['footercolumnrightcontent'],
            $output
        );

        $templatecontext['footercolumnaccreditationstitle'] = format_string($templatecontext['footercolumnaccreditationstitle'], true);
        $templatecontext['footercolumnaccreditationscontent'] = self::get_accreditation_links();

        $templatecontext['enablemobilewebservice'] = $CFG->enablemobilewebservice;

        if ($CFG->enablemobilewebservice) {
            $iosappid = get_config('tool_mobile', 'iosappid');
            if (!empty($iosappid)) {
                $templatecontext['iosappid'] = $iosappid;
            }

            $androidappid = get_config('tool_mobile', 'androidappid');
            if (!empty($androidappid)) {
                $templatecontext['androidappid'] = $androidappid;
            }

            $setuplink = get_config('tool_mobile', 'setuplink');
            if (!empty($setuplink)) {
                $templatecontext['mobilesetuplink'] = $setuplink;
            }
        }

        return $templatecontext;
    }

}
