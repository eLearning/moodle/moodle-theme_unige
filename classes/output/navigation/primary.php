<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace theme_unige\output\navigation;

use core\navigation\output\primary as OutputPrimary;
use renderable;
use renderer_base;
use templatable;

/**
 * Primary navigation renderable.
 *
 * @package    theme_unige
 * @copyright  2023, Université de Genève <moodle@unige.ch>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class primary extends OutputPrimary implements renderable, templatable {
    /**
     * Get/Generate the user menu.
     *
     * This is leveraging the data from user_get_user_navigation_info and the logic in $OUTPUT->user_menu()
     * We override this method to be able to insert the accessibility menu where we want in the user menu
     *
     * @param renderer_base $output
     * @return array
     */
    public function get_user_menu(renderer_base $output): array {
        global $CFG, $USER, $PAGE;

        require_once($CFG->dirroot . '/user/lib.php');

        $info = user_get_user_navigation_info($USER, $PAGE);
        if (isset($info->unauthenticateduser)) {
            $info->unauthenticateduser['content'] = get_string($info->unauthenticateduser['content']);
            $info->unauthenticateduser['url'] = get_login_url();
            $additionalloginlink = new additional_login_link($PAGE);
            $info->unauthenticateduser['additionalloginlinks'] = $additionalloginlink->export_for_template($output);
            $info->unauthenticateduser['hasalternatelogins'] = (empty($info->unauthenticateduser['additionalloginlinks'])) ? 0 : 1;
            return (array) $info;
        }

        $usermenudata = (parent::get_user_menu($output));

        $currentfont = get_user_preferences('accessibilitystyles_sitefont', 'default');

        $accessibilitymenu = [
            'title' => get_string('accessibilitymenu', 'theme_unige'),
            'items' => [
                0 => [
                    'title' => get_string('accessibilitydefaultfont', 'theme_unige'),
                    'text' => get_string('accessibilitydefaultfont', 'theme_unige'),
                    'link' => true,
                    'isactive' => ($currentfont == 'default'),
                    'url' => new \moodle_url('#'),
                    'attributes' => [
                        [
                            'key' => 'data-sitefont',
                            'value' => 'default',
                        ]
                    ],
                ],
                1 => [
                    'title' => get_string('accessibilitydyslexicfont', 'theme_unige'),
                    'text' => get_string('accessibilitydyslexicfont', 'theme_unige'),
                    'link' => true,
                    'isactive' => ($currentfont == 'odafont'),
                    'url' => new \moodle_url('#'),
                    'attributes' => [
                        [
                            'key' => 'data-sitefont',
                            'value' => 'odafont',
                        ]
                    ],
                ]
            ]
        ];

        // Include the language menu as a submenu within the user menu.
        // We use it here just to know if there is a language menu or not.
        $languagemenu = new \core\output\language_menu($PAGE);
        $langmenu = $languagemenu->export_for_template($output);

        $accessibilityitems = $accessibilitymenu['items'];
        // If there are available languages, generate the data for the the language selector submenu.
        if (!empty($accessibilityitems)) {
            $accessibilitysubmenuid = uniqid();
            // Generate the data for the link to language selector submenu.
            $accessibility = (object) [
                'itemtype' => 'submenu-link',
                'submenuid' => $accessibilitysubmenuid,
                'title' => get_string('accessibilitymenu', 'theme_unige'),
                'divider' => false,
                'submenulink' => true,
            ];

            // Place the link before the 'Preferences' menu item which is either the second to last,
            // third to last when 'Switch roles' or 'Languages' are available
            // or fourth to last if both are.
            // We start the offset at one because we don't want the Link to go after the 'Log out' menu item.
            $offset = 1;
            // Add 1 to the offset if the 'Switch roles' menu item is available.
            if (has_capability('moodle/role:switchroles', $PAGE->context)) {
                $offset = $offset + 1;
            }
            // Add 1 to the offset if the 'Language' menu item is available.
            if (!empty($langmenu)) {
                $offset = $offset + 1;
            }
            // Arbitrarily add 1 to the offset to go above the 'Preferences' menu item.
            $offset = $offset + 1;

            $menuposition = count($usermenudata['items']) - $offset;
            array_splice($usermenudata['items'], $menuposition, 0, [$accessibility]);

            // Generate the data for the accessibility selector submenu.
            $submenusdata = (object)[
                'id' => $accessibilitysubmenuid,
                'title' => get_string('accessibilityselector', 'theme_unige'),
                'items' => $accessibilityitems,
            ];
        }

        // Add divider before the last item.
        $usermenudata['items'][count($usermenudata['items']) - 2]->divider = true;
        $usermenudata['submenus'][] = $submenusdata;

        return $usermenudata;
    }
}
