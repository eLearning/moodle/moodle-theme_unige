<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace theme_unige\output\navigation;

use custom_menu;
use renderable;
use renderer_base;
use stdClass;
use templatable;
use theme_config;

/**
 * additional login links navigation renderable
 *
 * @package     unige
 * @category    navigation
 * @copyright   2023, Université de Genève
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class additional_login_link implements renderable, templatable {
    /** @var moodle_page $page the moodle page that the navigation belongs to */
    private $page = null;
    /**
     * @var \stdClass $theme The theme object.
     */
    protected $theme;

    /**
     * primary constructor.
     * @param \moodle_page $page
     */
    public function __construct($page) {
        $this->page = $page;
        $this->theme = theme_config::load('unige');
    }

    /**
     * Magic method to get theme settings
     *
     * @param string $name
     *
     * @return false|string|null
     */
    public function __get(string $name) {
        if (empty($this->theme->settings->$name)) {
            return false;
        }

        return $this->theme->settings->$name;
    }

    /**
     * Describe the renderable widget so it can be renderer by a mustache template.
     *
     * @param renderer_base $output
     * @return array
     */
    public function export_for_template(renderer_base $output) {
        $templatecontext = [];

        $settings = [
            'additionalloginlink',
        ];

        foreach ($settings as $setting) {
            $templatecontext[$setting] = $this->$setting;
        }

        $custommenu = new custom_menu();
        $menucollection = $custommenu->convert_text_to_menu_nodes($templatecontext['additionalloginlink'], current_language());

        $links = [];

        foreach ($menucollection as $menu) {
            $links[] = $menu->export_for_template($output);
        }

        return $links;
    }

}
