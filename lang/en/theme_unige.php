<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Language file.
 *
 * @package    theme_unige
 * @copyright  2023, Université de Genève <moodle@unige.ch>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['choosereadme'] = 'Theme Unige is a child theme of Boost. Made by and for the University of Geneva.';
$string['configtitle'] = 'Unige';
$string['generalsettings'] = 'General settings';
$string['pluginname'] = 'Unige';
$string['region-side-pre'] = 'Right';
$string['unaddableblocks'] = 'Unneeded blocks';
$string['unaddableblocks_desc'] = 'The blocks specified are not needed when using this theme and will not be listed in the \'Add a block\' menu.';
$string['preset'] = 'Theme preset';
$string['preset_desc'] = 'Pick a preset to broadly change the look of the theme.';
$string['presetfiles'] = 'Additional theme preset files';
$string['presetfiles_desc'] = 'Preset files can be used to dramatically alter the appearance of the theme. See <a href="https://docs.moodle.org/dev/Boost_Presets">Boost presets</a> for information on creating and sharing your own preset files, and see the <a href="https://archive.moodle.net/boost">Presets repository</a> for presets that others have shared.';
$string['loginbackgroundimage'] = 'Login page background image';
$string['loginbackgroundimage_desc'] = 'The image to display as a background for the login page.';
$string['backgroundimage'] = 'Background image';
$string['backgroundimage_desc'] = 'The image to display as a background of the site. The background image you upload here will override the background image in your theme preset files.';
$string['brandcolor'] = 'Brand colour';
$string['brandcolor_desc'] = 'The accent colour.';
$string['scrollspysetting'] = 'Scroll-spy';
$string['scrollspysetting_desc'] = 'With this setting, upon toggling edit mode on and off, the scroll position at where the user was when performing the toggle is preserved.';
$string['advancedsettings'] = 'Advanced settings';
$string['rawscss'] = 'Raw SCSS';
$string['rawscss_desc'] = 'Use this field to provide SCSS or CSS code which will be injected at the end of the style sheet.';
$string['rawscsspre'] = 'Raw initial SCSS';
$string['rawscsspre_desc'] = 'In this field you can provide initialising SCSS code, it will be injected before everything else. Most of the time you will use this setting to define variables.';
$string['headersettings'] = 'Header';
$string['navigationmenu'] = 'Navigation menu';
$string['navigationmenu_desc'] = "The navigation menu is the menu accessible from the header for navigating between important Moodle pages.
Enter above the list of internal Moodle URLs to display in the navigation menu. A Moodle internal URL is the URL of a Moodle page without the site root. For example, for the 'Course Catalog' page the internal Moodle URL is '/course'. URLs must be separated by ; (semicolons).
Theme translation files must be updated manually. Each menu link has a translation identifier in the form 'header-navigation-menu-link-n', the 'n' being the position of the link. Thus, the first link in the list has the translation identifier 'header-navigation-menu-link-1', the second 'header-navigation-menu-link-2', etc.";
$string['darkmodetoggledisplay'] = 'Display dark mode toggle';
$string['darkmodetoggledisplay_desc'] = 'If enabled, display the dark mode toggle button in the navbar';
$string['navbarcolor'] = 'Navbar color';
$string['navbarcolor_desc'] = 'The color scheme of the main navbar';
$string['light'] = 'Light';
$string['dark'] = 'Dark';
$string['removedprimarynavitems'] = 'Unneeded primary links';
$string['removedprimarynavitems_desc'] = 'Links to be excluded from the primary navigation';
$string['additionalloginlink'] = 'Additional login links';
$string['additionalloginlink_desc'] = 'Enter each menu item on a new line with format: menu text, a link URL (optional, not for a top menu item with sub-items), a tooltip title (optional) and a language code or comma-separated list of codes (optional, for displaying the line to users of the specified language only),
separated by pipe characters. Lines starting with a hyphen will appear as menu items in the previous top level menu and ### makes a divider. For example:
<pre>Alternate login | https://example.com/login1
Another alternate login | https://example.com/login2 || en
</pre>';
$string['accessibilitymenu'] = 'Accessibility';
$string['accessibilitydefaultfont'] = 'Default font';
$string['accessibilitydyslexicfont'] = 'Dyslexic font';
$string['accessibilityselector'] = 'Accessibility selector';
$string['footersettings'] = 'Footer';
$string['footercolumnleftdisplay'] = 'Display the left column';
$string['footercolumnleftdisplay_desc'] = 'If enabled, display the left column in the footer.';
$string['footercolumnlefttitle'] = 'Left column title';
$string['footercolumnlefttitle_desc'] = 'Title of the left column. The multilang filter can be used here.';
$string['footercolumnleftcontent'] = 'Left column content';
$string['footercolumnleftcontent_desc'] = 'Content is displayed as a list. Enter each item on a new line with format: text, a link URL, a title (optional) and a language code (optional). Lines starting with a hyphen will appear as menu items in the previous top level menu and ### makes a divider. For example:
<pre>Link 1 | https://example.com/something
Another link | https://example.com/anotherthing || en
</pre>';
$string['footercolumnmiddledisplay'] = 'Display the middle column';
$string['footercolumnmiddledisplay_desc'] = 'If enabled, display the middle column in the footer.';
$string['footercolumnmiddletitle'] = 'Middle column title';
$string['footercolumnmiddletitle_desc'] = 'Title of the middle column. The multilang filter can be used here.';
$string['footercolumnmiddlecontent'] = 'Middle column content';
$string['footercolumnmiddlecontent_desc'] = 'Content is displayed as a list. Enter each item on a new line with format: text, a link URL, a title (optional) and a language code (optional). Lines starting with a hyphen will appear as menu items in the previous top level menu and ### makes a divider. For example:
<pre>Link 1 | https://example.com/something
Another link | https://example.com/anotherthing || en
</pre>';
$string['footercolumnrightdisplay'] = 'Display the right column';
$string['footercolumnrightdisplay_desc'] = 'If enabled, display the right column in the footer.';
$string['footercolumnrighttitle'] = 'Right column title';
$string['footercolumnrighttitle_desc'] = 'Title of the right column. The multilang filter can be used here.';
$string['footercolumnrightcontent'] = 'Right column content';
$string['footercolumnrightcontent_desc'] = 'Content is displayed as a list. Enter each item on a new line with format: text, a link URL, a title (optional) and a language code (optional). Lines starting with a hyphen will appear as menu items in the previous top level menu and ### makes a divider. For example:
<pre>Link 1 | https://example.com/something
Another link | https://example.com/anotherthing || en
</pre>';
$string['footercolumnaccreditationsdisplay'] = 'Display the accreditations column';
$string['footercolumnaccreditationsdisplay_desc'] = 'If enabled, display the accreditations column in the footer.';
$string['footercolumnaccreditationstitle'] = 'Accreditations column title';
$string['footercolumnaccreditationstitle_desc'] = 'Title of the accreditations column. The multilang filter can be used here.';
$string['footercolumnaccreditationscontent'] = 'Accreditations column content';
$string['footercolumnaccreditationscontent_desc'] = 'Content is displayed as a list in alphabetical order by filename.';
$string['footercolumnmobiletitle'] = "MOODLE MOBILE APP";
$string['toggledarkmode'] = 'Toggle dark mode';

// Participant list info text.
$string['inforesetstudents'] = '<b>Caution:</b> to unenroll all students, use the "reset" function. More info <a target=_blank href="https://docs.moodle.org/401/en/Reset_course">here</a>.';
