<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   theme_unige
 * @copyright 2023, Université de Genève <moodle@unige.ch>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($ADMIN->fulltree) {
    $settings = new theme_boost_admin_settingspage_tabs('themesettingunige', get_string('configtitle', 'theme_unige'));
    $page = new admin_settingpage('theme_unige_general', get_string('generalsettings', 'theme_unige'));

    // Unaddable blocks.
    // Blocks to be excluded when this theme is enabled in the "Add a block" list: Administration, Navigation, Courses and
    // Section links.
    $default = 'navigation,settings,course_list,section_links';
    $setting = new admin_setting_configtext('theme_unige/unaddableblocks',
        get_string('unaddableblocks', 'theme_unige'), get_string('unaddableblocks_desc', 'theme_unige'), $default, PARAM_TEXT);
    $page->add($setting);

    // Preset.
    $name = 'theme_unige/preset';
    $title = get_string('preset', 'theme_unige');
    $description = get_string('preset_desc', 'theme_unige');
    $default = 'default.scss';

    $context = context_system::instance();
    $fs = get_file_storage();
    $files = $fs->get_area_files($context->id, 'theme_unige', 'preset', 0, 'itemid, filepath, filename', false);

    $choices = [];
    foreach ($files as $file) {
        $choices[$file->get_filename()] = $file->get_filename();
    }
    // These are the built in presets.
    $choices['default.scss'] = 'default.scss';
    $choices['plain.scss'] = 'plain.scss';

    $setting = new admin_setting_configthemepreset($name, $title, $description, $default, $choices, 'unige');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Preset files setting.
    $name = 'theme_unige/presetfiles';
    $title = get_string('presetfiles', 'theme_unige');
    $description = get_string('presetfiles_desc', 'theme_unige');

    $setting = new admin_setting_configstoredfile($name, $title, $description, 'preset', 0,
        array('maxfiles' => 20, 'accepted_types' => array('.scss')));
    $page->add($setting);

    // Background image setting.
    $name = 'theme_unige/backgroundimage';
    $title = get_string('backgroundimage', 'theme_unige');
    $description = get_string('backgroundimage_desc', 'theme_unige');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'backgroundimage');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Login Background image setting.
    $name = 'theme_unige/loginbackgroundimage';
    $title = get_string('loginbackgroundimage', 'theme_unige');
    $description = get_string('loginbackgroundimage_desc', 'theme_unige');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'loginbackgroundimage');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Variable $body-color.
    // We use an empty default value because the default colour should come from the preset.
    $name = 'theme_unige/brandcolor';
    $title = get_string('brandcolor', 'theme_unige');
    $description = get_string('brandcolor_desc', 'theme_unige');
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Setting: scroll-spy.
    $name = 'theme_unige/scrollspy';
    $title = get_string('scrollspysetting', 'theme_unige', null, true);
    $description = get_string('scrollspysetting_desc', 'theme_unige', null, true);
    $default = '1';
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Must add the page after definiting all the settings!
    $settings->add($page);

    /*
    * --------------------
    * Header settings tab
    * --------------------
    */
    $page = new admin_settingpage('theme_unige_header', get_string('headersettings', 'theme_unige'));

    // Header navbar color.
    $name = 'theme_unige/navbarcolor';
    $title = get_string('navbarcolor', 'theme_unige');
    $description = get_string('navbarcolor_desc', 'theme_unige');
    $choices = [
        '0' => get_string('light', 'theme_unige'),
        '1' => get_string('dark', 'theme_unige'),
    ];
    $setting = new admin_setting_configselect($name, $title, $description, 'light', $choices);
    $page->add($setting);

    // Header display darkmode toggle button.
    $name = 'theme_unige/darkmodetoggledisplay';
    $title = get_string('darkmodetoggledisplay', 'theme_unige');
    $description = get_string('darkmodetoggledisplay_desc', 'theme_unige');
    $default = '0';
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $page->add($setting);

    // Links to be excluded from the primary navigation.
    $default = 'home,courses';
    $setting = new admin_setting_configtext('theme_unige/removedprimarynavitems',
        get_string('removedprimarynavitems', 'theme_unige'), get_string('removedprimarynavitems_desc', 'theme_unige'), $default, PARAM_TEXT);
    $page->add($setting);

    // Additional login link.
    $name = 'theme_unige/additionalloginlink';
    $title = get_string('additionalloginlink', 'theme_unige');
    $description = get_string('additionalloginlink_desc', 'theme_unige');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $page->add($setting);

    $settings->add($page);

    /*
    * --------------------
    * Footer settings tab
    * --------------------
    */
    $page = new admin_settingpage('theme_unige_footer', get_string('footersettings', 'theme_unige'));

    // Footer column left display.
    $name = 'theme_unige/footercolumnleftdisplay';
    $title = get_string('footercolumnleftdisplay', 'theme_unige');
    $description = get_string('footercolumnleftdisplay_desc', 'theme_unige');
    $default = '0';
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $page->add($setting);

    // Footer column left title.
    $name = 'theme_unige/footercolumnlefttitle';
    $title = get_string('footercolumnlefttitle', 'theme_unige');
    $description = get_string('footercolumnlefttitle_desc', 'theme_unige');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $page->add($setting);

    // Footer column left content.
    $name = 'theme_unige/footercolumnleftcontent';
    $title = get_string('footercolumnleftcontent', 'theme_unige');
    $description = get_string('footercolumnleftcontent_desc', 'theme_unige');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $page->add($setting);

    // Footer column middle display.
    $name = 'theme_unige/footercolumnmiddledisplay';
    $title = get_string('footercolumnmiddledisplay', 'theme_unige');
    $description = get_string('footercolumnmiddledisplay_desc', 'theme_unige');
    $default = '0';
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $page->add($setting);

    // Footer column middle title.
    $name = 'theme_unige/footercolumnmiddletitle';
    $title = get_string('footercolumnmiddletitle', 'theme_unige');
    $description = get_string('footercolumnmiddletitle_desc', 'theme_unige');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $page->add($setting);

    // Footer column middle content.
    $name = 'theme_unige/footercolumnmiddlecontent';
    $title = get_string('footercolumnmiddlecontent', 'theme_unige');
    $description = get_string('footercolumnmiddlecontent_desc', 'theme_unige');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $page->add($setting);

    // Footer column right display.
    $name = 'theme_unige/footercolumnrightdisplay';
    $title = get_string('footercolumnrightdisplay', 'theme_unige');
    $description = get_string('footercolumnrightdisplay_desc', 'theme_unige');
    $default = '0';
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $page->add($setting);

    // Footer column right title.
    $name = 'theme_unige/footercolumnrighttitle';
    $title = get_string('footercolumnrighttitle', 'theme_unige');
    $description = get_string('footercolumnrighttitle_desc', 'theme_unige');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $page->add($setting);

    // Footer column right content.
    $name = 'theme_unige/footercolumnrightcontent';
    $title = get_string('footercolumnrightcontent', 'theme_unige');
    $description = get_string('footercolumnrightcontent_desc', 'theme_unige');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $page->add($setting);

    // Footer column accreditations display.
    $name = 'theme_unige/footercolumnaccreditationsdisplay';
    $title = get_string('footercolumnaccreditationsdisplay', 'theme_unige');
    $description = get_string('footercolumnaccreditationsdisplay_desc', 'theme_unige');
    $default = '0';
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $page->add($setting);

    // Footer column accreditations title.
    $name = 'theme_unige/footercolumnaccreditationstitle';
    $title = get_string('footercolumnaccreditationstitle', 'theme_unige');
    $description = get_string('footercolumnaccreditationstitle_desc', 'theme_unige');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $page->add($setting);

    // Footer column accreditations content.
    $name = 'theme_unige/footercolumnaccreditationscontent';
    $title = get_string('footercolumnaccreditationscontent', 'theme_unige');
    $description = get_string('footercolumnaccreditationscontent_desc', 'theme_unige');
    $default = '';
    $setting = new admin_setting_configstoredfile(
        $name,
        $title,
        $description,
        'accreditationimage',
        0,
        [
            'maxfiles' => 5,
            'accepted_types' => [
                'jpg',
                'jpeg',
                'png',
                'svg',
            ],
        ]
    );
    $page->add($setting);

    $settings->add($page);

    // Advanced settings.
    $page = new admin_settingpage('theme_unige_advanced', get_string('advancedsettings', 'theme_unige'));

    // Raw SCSS to include before the content.
    $setting = new admin_setting_scsscode('theme_unige/scsspre',
        get_string('rawscsspre', 'theme_unige'), get_string('rawscsspre_desc', 'theme_unige'), '', PARAM_RAW);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Raw SCSS to include after the content.
    $setting = new admin_setting_scsscode('theme_unige/scss', get_string('rawscss', 'theme_unige'),
        get_string('rawscss_desc', 'theme_unige'), '', PARAM_RAW);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    $settings->add($page);
}
