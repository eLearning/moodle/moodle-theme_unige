// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handle the back to top button.
 *
 * @module     theme_unige/darkmode-control
 * @copyright  2023, Université de Genève <moodle@unige.ch>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
import Ajax from 'core/ajax';
import Notification from 'core/notification';

export const init = () => {
    const toggle = document.querySelector('[data-action="darkmode-control"]');

    /**
     * Fetch site color from user preferences
     * @returns {Promise}
     */
    const getSiteColor = () => Ajax.call([{
            methodname: 'core_user_get_user_preferences',
            args: {
                name: 'accessibilitystyles_sitecolor',
            },
        }])[0]
        .then((result) => result.preferences[0].value);

    /**
     * Change the 'color' attribute on the body tag
     * @param {string} color The current state of the color attribute.
     */
    function changeSiteColor(color) {
        let html = document.documentElement;

        var request = {
            methodname: 'theme_unige_sitecolor',
            args: {
                color: color
            },
        };

        Ajax.call([request])[0]
            .done(function(data) {
                if (!data.success) {
                    Notification.addNotification({
                        message: data.warnings[0].message,
                        type: 'error'
                    });
                } else {
                    html.setAttribute('data-theme', color);
                }
            })
            .fail(Notification.exception);
    }

    toggle.addEventListener('click', async event => {
        event.preventDefault();
        const color = await getSiteColor();
        if (color === 'dark') {
            changeSiteColor('light');
        } else {
            changeSiteColor('dark');
        }
    });
};
