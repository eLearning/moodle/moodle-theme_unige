// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handle the back to top button.
 *
 * @module     theme_unige/footer-back-to-top
 * @copyright  2023, Université de Genève <moodle@unige.ch>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

export const init = () => {
    const button = document.querySelector('[data-action="footer-back-to-top"]');
    const page = document.documentElement;

    button.addEventListener('click', () => {
        page.scrollTo({top: 0, behavior: 'smooth'});
    });

    document.addEventListener('scroll', () => {
        if (page.scrollTop > 20) {
            button.style.display = 'block';
        } else {
            button.style.display = 'none';
        }
    });
};
