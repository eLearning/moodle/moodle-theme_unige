// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handle the back to top button.
 *
 * @module     theme_unige/dyslexic-control
 * @copyright  2023, Université de Genève <moodle@unige.ch>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
import Ajax from 'core/ajax';
import Notification from 'core/notification';

export const init = () => {
    const fontDefault = document.querySelector('[data-sitefont="default"]');
    const fontDyslexic = document.querySelector('[data-sitefont="odafont"]');

    /**
     * Fetch dyslexic setting from user preferences
     * @returns {Promise}
     */
    // const getDyslexicSetting = () => Ajax.call([{
    //         methodname: 'core_user_get_user_preferences',
    //         args: {
    //             name: 'accessibilitystyles_sitefont',
    //         },
    //     }])[0]
    //     .then((result) => result.preferences[0].value);

    /**
     * Change the 'font' attribute on the body tag
     * @param {boolean} dyslexic The current state of the dyslexic attribute.
     */
    function changeSiteFont(dyslexic) {
        let body = document.body;

        var request = {
            methodname: 'theme_unige_sitefont',
            args: {
                font: dyslexic
            },
        };

        Ajax.call([request])[0]
            .done(function(data) {
                if (!data.success) {
                    Notification.addNotification({
                        message: data.warnings[0].message,
                        type: 'error'
                    });
                } else {
                    if (dyslexic === 'odafont') {
                        body.classList.add('odafont');
                        fontDefault.removeAttribute('aria-current');
                        fontDyslexic.setAttribute('aria-current', true);
                    } else {
                        body.classList.remove('odafont');
                        fontDefault.setAttribute('aria-current', true);
                        fontDyslexic.removeAttribute('aria-current');
                    }
                }
            })
            .fail(Notification.exception);
    }

    if (fontDefault) {
        fontDefault.addEventListener('click', async event => {
            event.preventDefault();
            changeSiteFont('default');
        });
    }

    if (fontDyslexic) {
        fontDyslexic.addEventListener('click', async event => {
            event.preventDefault();
            changeSiteFont('odafont');
        });
    }

};
