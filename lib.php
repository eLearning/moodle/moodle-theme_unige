<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Theme functions.
 *
 * @package    theme_unige
 * @copyright  2023, Université de Genève <moodle@unige.ch>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Serves any files associated with the theme settings.
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param context $context
 * @param string $filearea
 * @param array $args
 * @param bool $forcedownload
 * @param array $options
 * @return bool
 */
function theme_unige_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = array()) {
    if ($context->contextlevel == CONTEXT_SYSTEM && ($filearea === 'logo' || $filearea === 'backgroundimage' ||
        $filearea === 'loginbackgroundimage' || $filearea === 'accreditationimage')) {
        $theme = theme_config::load('unige');
        // By default, theme files must be cache-able by both browsers and proxies.
        if (!array_key_exists('cacheability', $options)) {
            $options['cacheability'] = 'public';
        }
        return $theme->setting_file_serve($filearea, $args, $forcedownload, $options);
    } else {
        send_file_not_found();
    }
}

/**
 * Returns the main SCSS content.
 *
 * @param theme_config $theme The theme config object.
 * @return string
 */
function theme_unige_get_main_scss_content($theme) {
    global $CFG;

    $scss = '';
    $filename = !empty($theme->settings->preset) ? $theme->settings->preset : null;
    $fs = get_file_storage();

    $context = context_system::instance();
    if ($filename == 'default.scss') {
        $scss .= file_get_contents($CFG->dirroot . '/theme/boost/scss/preset/default.scss');
    } else if ($filename == 'plain.scss') {
        $scss .= file_get_contents($CFG->dirroot . '/theme/boost/scss/preset/plain.scss');
    } else if ($filename && ($presetfile = $fs->get_file($context->id, 'theme_unige', 'preset', 0, '/', $filename))) {
        $scss .= $presetfile->get_content();
    } else {
        // Safety fallback - maybe new installs etc.
        $scss .= file_get_contents($CFG->dirroot . '/theme/boost/scss/preset/default.scss');
    }

    // Unige scss.
    $unigevariables = file_get_contents($CFG->dirroot . '/theme/unige/scss/unige/_variables.scss');
    $unige = file_get_contents($CFG->dirroot . '/theme/unige/scss/default.scss');

    // Combine them together.
    $allscss = $unigevariables . PHP_EOL . $scss . PHP_EOL . $unige;

    return $allscss;
}

/**
 * Renders extra item in the navbar.
 *
 * @param renderer_base $renderer
 * @return string The HTML
 */
function theme_unige_render_navbar_output(\renderer_base $renderer) {
    global $PAGE;

    $output = '';
    $context = [];

    if ($PAGE->theme->name === 'unige' && isloggedin() && !isguestuser() && get_config('theme_unige', 'darkmodetoggledisplay')) {
        $output .= $renderer->render_from_template('theme_unige/darkmode_toggle', $context);
    }

    return $output;
}

/**
 * @deprecated Moodle 4.4 - The Hook interfaces replaces this callback.
 * Add custom attributes to the html tag for dark mode.
 */
function theme_unige_add_htmlattributes() {
    global $PAGE;

    if ($PAGE->theme->name !== 'unige') {
        return [];
    }

    return [
        'data-theme' => get_user_preferences('accessibilitystyles_sitecolor', 'light'),
    ];
}
