# University of Geneva Boost child theme

*Author : University of Geneva, 2018*


Boost Child theme for Moodle 4.x

A related Mahara theme can be found [in our Gitlab](https://gitlab.unige.ch/portfolio/Theme-Mahara)

## Installation 

Copy under the following path [MyMoodle]/theme/unige 

## Configuration and settings

### To change colors for the top categories : 

`Administration -> Appearance -> Themes -> Unige -> Advanced settings -> Raw initial SCSS`

Add the following:
```
$fac-sciences:    #007e64 !default;
$fac-medecine:    #96004b !default;
$fac-lettres:     #0067c5 !default;
$fac-societe:     #f1ab00 !default;
$fac-droit:       #f42941 !default;
$fac-theologie:   #4b0b71 !default;
$fac-psychologie: #00b1ae !default;
$fac-economie:    #465f7f !default;
$fac-traduction:  #ff5c00 !default;

$categories: map-merge(
    (
        1: $fac-sciences,
        2: $fac-medecine,
        3: $fac-lettres,
        4: $fac-societe,
        5: $fac-economie,
        6: $fac-droit,
        7: $fac-theologie,
        8: $fac-psychologie,
        9: $fac-traduction,
    ),
    $categories
);
```

The key of the map corresponds to the id of the categories, you can use other variables name if you want to.
If a category doesn't have a specific color assigned the primary brand color will be used.

### Header

#### Navbar color
Color scheme of the main navbar

#### Dark mode
Display the toggle for the dark mode. 
CAUTION: this is not ready for production, there is still missing css on several components that will break the display (ie. white text on white background).

#### Additional login links
These are not login methods, just links, for example if you use an IDP like Shibboleth you can put direct links to a specific organisation login in this menu. It will be displayed in a dropdown next to the `log in` link in the navbar

### Footer

#### Left column and Middle column
Can display a list of links.

#### Right column
This column will display the links to download the Moodle mobile app.
They will be displayed for iOS and Android if the respective identifier in `Administration -> General -> Mobile app -> Mobile appearance -> App Banners -> iOS/Android identifier` are set.
